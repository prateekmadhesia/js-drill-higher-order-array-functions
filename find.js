function find(elements, callback){
  let result =undefined;
  /*
    This condition is to check the arguments of this function, if data type of arguments is not proper we just return.
  */
  if(elements==undefined || callback==undefined  || typeof(elements)!='object' || typeof(callback)!='function' || elements.length==0){
    return result;
  }
  
  for(let i=0; i<elements.length; i++){
    if(callback(elements[i], i, elements)){
      result = elements[i];
      return result;
    }
  }
  return result;
}
module.exports=find;