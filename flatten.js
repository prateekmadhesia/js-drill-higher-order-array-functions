function flat(elements){
  const result =[];
  /*
    This condition is to check the arguments of this function, if data type of arguments is not proper we just return.
  */
  if(elements==undefined || typeof(elements)!='object' || elements.length==0){
    return result;
  }
  
  for(let i=0; i<elements.length; i++){

    if(Array.isArray(elements[i])){
      helper(elements[i], result); 
      continue;
    }

    result.push(elements[i]);
  }
  return result;
}

// This is be our recursive function.
function helper(array, arrayToAdd){

  for(let i=0; i<array.length; i++){

    if(Array.isArray(array[i])){
      helper(array[i], arrayToAdd); // recursive call
      continue;
    }

    arrayToAdd.push(array[i]);
  }
  return;
}
module.exports=flat;