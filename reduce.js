function reduce(elements, callback, startingValue){
  /*
    This condition is to check the arguments of this function, if data type of arguments is not proper we just return.
  */
  if(elements==undefined || callback==undefined  || typeof(elements)!='object' || typeof(callback)!='function' || elements.length==0){
    return undefined;
  }
  
  for(let i=0; i<elements.length; i++){

    if(startingValue==undefined){
      startingValue=elements[i];
      continue;
    }
    startingValue = callback(startingValue, elements[i], i, elements);
    
  }
  return startingValue;
}
module.exports=reduce;