const problem1 = require('../each.js');

const items = [1, 2, 3, 4, 5, 5]; //Array
console.log(problem1);
function print(ele, inx){ // This is our callback function which console each value of the array.
  console.log(ele, inx);
}

problem1(items, print); 

problem1(items, (valueToDouble) => { // This function will double each value of the array.
  console.log(valueToDouble * 2);
}); 