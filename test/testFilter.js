const problemFilter = require('../filter.js');

const items = [1, 2, 3, 4, 5, 5]; //Array

// This is our callback function which tell whether the value of array is odd or not.
function odd(ele) {
  if (ele % 2 == 0) {
    return false;
  }
  return true;
}

const output1 = problemFilter(items, odd);

// This function tell us whether the value of array is even or not.
const output2 = problemFilter(items, (ele) => {
  if (ele % 2 == 0) {
    return true;
  }
  return false;

});

console.log(output1);
console.log(output2);
