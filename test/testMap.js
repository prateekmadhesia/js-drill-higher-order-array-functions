const problemMap = require('../map.js');

const items = [1, 2, 3, 4, 5, 5]; //Array

// This is our callback function which double every value of the array.
function double(value){ 
  return 2 * value;
}

// This is our callback function which give square root of every element of the array.
function squareRoot(value){ 
  return Math.sqrt(value);
}

const output1 = problemMap(items, double); 

// This is our callback function which multiply every value with its index of the array.
const output2 = problemMap(items, (value, index, arr) => { 
  console.log("original array" + arr);
  return value * index;
}); 

const output3 =problemMap(items, squareRoot); 

console.log(output1);
console.log(output2);
console.log(output3);