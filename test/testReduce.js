const problemReduce = require('../reduce.js');

const items = [1, 2, 3, 4, 5, 5]; //Array

// This is our callback function which help to get sum of all value from the array.
function sumOfAllElements(accumulator, currentValue){ 
  return accumulator + currentValue;
}

// This is our callback function which help to get max value from the array.
function maxElements(accumulator, currentValue){ 
  return accumulator<currentValue ? currentValue : accumulator;
}

const output1 =problemReduce(items, sumOfAllElements); 

// This is our callback function which help to get sum of all value with initial value from the array.
const output2 = problemReduce(items, (acc, curr, index, array) => { 
  console.log(index, array);
  return acc + curr;
}, 10); 

const output3 =problemReduce(items, maxElements); 

console.log(output1);
console.log(output2);
console.log(output3);