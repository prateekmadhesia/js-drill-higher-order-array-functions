const problemFlatten = require('../flatten.js');

const nestedArray = [1, [2], [[3]], [[[4]]]]; //Array

// This will flat our nested array.
const output =problemFlatten(nestedArray); 

console.log(output);
