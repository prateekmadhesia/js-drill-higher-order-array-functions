const problemFind = require('../find.js');

const items = [1, 2, 3, 4, 5, 5]; //Array

// This is our callback function which help to find a value from the array.
function findANumber(ele){ 
  if(ele==3){
    return true;
  }
}

const output1 =problemFind(items, findANumber); 

// This function will find value greater then 4 in the array.
const output2 = problemFind(items, (value) => { 
  if( value > 4){
    return true;
  }
}); 

console.log(output1);
console.log(output2);
