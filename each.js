function each(elements, callback){

  /*
    This condition is to check the arguments of this function, if data type of arguments is not proper we just return.
  */
  if(elements==undefined || callback==undefined  || typeof(elements)!='object' || typeof(callback)!='function' || elements.length==0){
    return;
  }
  
  for(let i=0; i<elements.length; i++){
    callback(elements[i], i, elements);
  }

}
module.exports=each;